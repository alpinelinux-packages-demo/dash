# dash

POSIX-compliant shell http://gondor.apana.org.au/~herbert/dash/

* [dash](https://en.wikipedia.org/wiki/Almquist_shell#dash) @ Wikipedia

# GitLab CI/CD
In the context of GitLab CI/CD, `dash` is a shell which does not support `set -o pipefail`. Because of this it is almost impossible to use it under GitLab CI/CD supervision using `.gitlab-ci.yml` (written in 2020).
